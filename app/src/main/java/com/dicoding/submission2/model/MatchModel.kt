package com.dicoding.submission2.model

data class MatchModel(
    var idEvent: String? = null,
    var dateEvent: String? = null,
    var strHomeTeam: String? = null,
    var strAwayTeam: String? = null,
    var intHomeScore: String? = null,
    var intAwayScore: String? = null
)