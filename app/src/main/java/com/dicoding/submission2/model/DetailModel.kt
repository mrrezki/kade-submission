package com.dicoding.submission2.model

data class DetailModel(
    var idEvent: String,
    var dateEvent: String,
    var strTime: String,
    var idHomeTeam: String,
    var idAwayTeam: String,
    var intHomeScore: String,
    var intAwayScore: String,
    var strHomeGoalDetails: String,
    var strAwayGoalDetails: String

)