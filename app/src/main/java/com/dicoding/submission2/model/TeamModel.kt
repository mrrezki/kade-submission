package com.dicoding.submission2.model

data class TeamModel(
    var name: String? = null,
    var emblem: String? = null
)